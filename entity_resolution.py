from pyspark.sql import SQLContext
from pyspark.sql import functions as sf
from pyspark import SparkConf, SparkContext
from pyspark.sql.types import FloatType, ArrayType, StringType

conf = SparkConf().setAppName('Entity Resolution')
sc = SparkContext(conf=conf)

sqlCt = SQLContext(sc)

def flat_token_keys(id_tokens):
    id_token_list = []
    for token in id_tokens[1]:
        id_token_list.append((id_tokens[0],token))
    return id_token_list

class EntityResolution:
    def __init__(self, dataFile1, dataFile2, stopWordsFile):
        self.f = open(stopWordsFile, "r")
        self.stopWords = set(self.f.read().split("\n"))
        self.stopWordsBC = sc.broadcast(self.stopWords).value
        self.df1 = sqlCt.read.parquet(dataFile1).cache()
        self.df2 = sqlCt.read.parquet(dataFile2).cache()

    def preprocessDF(self, df, cols): 
        
        df = df.withColumn('joinKey', sf.concat_ws(' ',df[cols[0]],df[cols[1]]))
        
        df = df.withColumn('joinKey', sf.lower(df.joinKey))

        df = df.withColumn('joinKey', sf.split('joinKey', r'\W+'))
        
        stopwords = self.stopWordsBC

        # the filter(bool, x) is just to remove empty string elements of the list of tokens
        remove_stopwords_udf = sf.udf(lambda x: list(set(filter(bool, x)).difference(stopwords)), ArrayType(StringType()))
        
        df = df.withColumn("joinKey", remove_stopwords_udf(df.joinKey))
        
        return df        
    
    
    def filtering(self, df1, df2):
        
        flat_token_list_df1 = df1.select(sf.col('id'),sf.col('joinKey')) \
                                 .rdd.map(flat_token_keys) \
                                 .flatMap(lambda token: token)
                
        flat_df1 = sqlCt.createDataFrame(flat_token_list_df1, ('id1', 'token1'))        
                       
        flat_token_list_df2 = df2.select(sf.col('id'),sf.col('joinKey')) \
                                 .rdd.map(flat_token_keys) \
                                 .flatMap(lambda token: token)

        flat_df2 = sqlCt.createDataFrame(flat_token_list_df2, ('id2', 'token2'))        

        # dataframe only with the ids with at least one matching token
        cand_ids_df = flat_df1.join(other = flat_df2, on=[flat_df1.token1 == flat_df2.token2]) \
                            .select(flat_df1.id1, flat_df2.id2) \
                            .dropDuplicates()
        
        # getting the joinKey1 from original dataframe df1
        cand_ids_joinKey1_df = cand_ids_df.join(other = df1, on=[cand_ids_df.id1 == df1.id]) \
                            .select(cand_ids_df.id1, df1.joinKey.alias('joinKey1'), cand_ids_df.id2) \
                            .dropDuplicates()
        
        # completing the dataframe with the joinKey2 from dataframe df2
        candDF = cand_ids_joinKey1_df.join(other = df2, on=[cand_ids_joinKey1_df.id2 == df2.id]) \
                            .select(cand_ids_joinKey1_df.id1, cand_ids_joinKey1_df.joinKey1, cand_ids_joinKey1_df.id2, df2.joinKey.alias('joinKey2')) \
                            .dropDuplicates()

        return candDF
    
        
    def verification(self, candDF, threshold):

        jaccard_udf = sf.udf(lambda x, y: round(len(set(x).intersection(set(y))) / len(set(x).union(set(y))),3), FloatType())
        resultDF = candDF.withColumn('jaccard', jaccard_udf(sf.col('joinKey1'), sf.col('joinKey2')))
        resultDF = resultDF.filter(resultDF.jaccard >= threshold)
        
        return resultDF
        
        

    def evaluate(self, result, groundTruth):
       
        precision = len(set(result).intersection(set(groundTruth))) / len(set(result))
        recall = len(set(result).intersection(set(groundTruth))) / len(set(groundTruth))
        fmeasure = (2 * precision * recall) / (precision + recall)
        
        return (precision, recall, fmeasure)
        

    def jaccardJoin(self, cols1, cols2, threshold):
        newDF1 = self.preprocessDF(self.df1, cols1)
        newDF2 = self.preprocessDF(self.df2, cols2)
        print("Before filtering: {} pairs in total".format(self.df1.count()*self.df2.count()))

        candDF = self.filtering(newDF1, newDF2)
        print("After Filtering: {} pairs left".format(candDF.count()))

        resultDF = self.verification(candDF, threshold)
        print("After Verification: {} similar pairs".format(resultDF.count()))

        return resultDF


    def __del__(self):
        self.f.close()
        
        
if __name__ == "__main__":

    er = EntityResolution("amazon-google/Amazon", "amazon-google/Google", "stopwords.txt")

    amazonCols = ["title", "manufacturer"]
    googleCols = ["name", "manufacturer"]
    resultDF = er.jaccardJoin(amazonCols, googleCols, 0.5)

    result = resultDF.rdd.map(lambda row: (row.id1, row.id2)).collect()

    groundTruth = sqlCt.read.parquet("amazon-google/Amazon_Google_perfectMapping") \
                          .rdd.map(lambda row: (row.idAmazon, row.idGoogle)).collect()

    
    print("(precision, recall, fmeasure) = ", er.evaluate(result, groundTruth))